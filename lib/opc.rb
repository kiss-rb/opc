# frozen_string_literal: true

require_relative "opc/version"

module Opc
  class Error < StandardError; end
  # Your code goes here...
end
